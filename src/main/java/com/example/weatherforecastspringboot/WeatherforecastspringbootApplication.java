package com.example.weatherforecastspringboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WeatherforecastspringbootApplication {

    public static void main(String[] args) {
        SpringApplication.run(WeatherforecastspringbootApplication.class, args);
    }

    //creating a new api call and initializing it
    /*ApiCall call = new ApiCall();
    call.init();*/


}
