package com.example.weatherforecastspringboot;


import com.example.weatherforecastspringboot.io.quicktype.*;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Stefan Kirkovski
 */

@Component
public class ApiCall {

    public static final String SKOPJE_COORDS = "lat=42&lon=21.433331";
    public static final String TETOVO_COORDS = "lat=42.010559&lon=20.97139";
    public static final String BITOLA_COORDS = "lat=41.031109&lon=21.340281";
    private static final String[] COORDS = {SKOPJE_COORDS, TETOVO_COORDS, BITOLA_COORDS};

    //@PostConstruct
    public void init() {

        String inline;
        for (int i = 0; i < COORDS.length; i++) {

            //making the apicall for a certain city and returning
            //a string with the api call JSON info
            inline = makeApiCall(i);
            //populating both databases with info taken from api
            //First argument is ResultMax arraylist, second one is ResultRain
            populateDatabases(returnCityDateMax(inline), returnCityDateWeather(inline));
        }
    }

    //Checking the max temp for the next 5 days and returning a resultmax arraylist
    public ArrayList<ResultMax> returnCityDateMax(String jsonString) {
        try {

            //We take the data from a JSON string, convert it to data we can use
            //and then move on to the iterator to check for the next 5 days
            ApiData data = Converter.fromJsonString(jsonString);
            Daily[] daily = data.getDaily();
            long lat = data.getLat();
            double lon = data.getLon();
            Temp temp;
            ArrayList<ResultMax> res = new ArrayList<>();
            int j = 0;
            Date date = new Date();
            LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

            //We need to get the next 5 days, so we always start from tomorrow
            for (int i = 1; i < 6; i++) {

                temp = daily[i].getTemp();
                //Checked with 20 Celsius, days ahead were too cold to check for 25
                if (temp.getMax() > 20) {
                    res.add(j++, new ResultMax(checkCity(lat, lon), (localDate.plusDays(i)), temp.getMax()));
                }
            }
            return res;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    //Checking the weather for the next 5 days and returning a resultrain arraylist
    public ArrayList<ResultRain> returnCityDateWeather(String jsonString) {
        try {

            //We take the data from a JSON string, convert it to data we can use
            //and then move on to the iterator to check for the next 5 days
            ApiData data = Converter.fromJsonString(jsonString);
            Daily[] daily = data.getDaily();
            long lat = data.getLat();
            double lon = data.getLon();
            Weather[] weather;
            String forecast;
            ArrayList<ResultRain> res = new ArrayList<>();
            int j = 0;
            Date date = new Date();
            LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

            //We need to get the next 5 days, so we always start from tomorrow
            for (int i = 1; i < 6; i++) {

                weather = daily[i].getWeather();
                forecast = weather[0].getMain();
                //Check if the weather forecast equals Rain
                if (forecast.contains("Rain")) {
                    res.add(j++, new ResultRain(checkCity(lat, lon), (localDate.plusDays(i)), forecast));
                }
            }
            return res;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    //We check if the lat and lon coords correspond to a certain city
    public String checkCity(long lat, double lon) {

        if (lat == 42 && lon == 21.43) {
            return "Skopje";
        } else if (lat == 42 && lon == 20.97) {
            return "Tetovo";
        } else if (lat == 41 && lon == 21.34) {
            return "Bitola";
        } else {
            return "Invalid City";
        }

    }

    //we check if the response code is 200(OK) and if so
    //we parse the info and return it back into a string
    public String returnScanner(int responseCode, URL url) {
        String inline = "";
        if (responseCode != 200) {
            throw new RuntimeException("HttpResponseCode: " + responseCode);
        } else {
            try {
                //Next part of the functionality

                Scanner sc = new Scanner(url.openStream());
                while (sc.hasNext()) {
                    inline += sc.nextLine();
                }
                sc.close();
                return inline;
            } catch (IOException ex) {
                Logger.getLogger(ApiCall.class.getName()).log(Level.SEVERE, null, ex);
                return null;
            }
        }
    }

    //We populate the databases (both for temp >20 and forecast == rain
    public void populateDatabases(ArrayList<ResultMax> resultInfo, ArrayList<ResultRain> resultInfo2) {

        try {
            String myUrl = "jdbc:h2:mem:stefan";
            Class.forName("org.h2.Driver");
            Connection connToDatabase = DriverManager.getConnection(myUrl, "sa", "");

            for (ResultMax rs : resultInfo) {


                // the mysql insert statement
                String query = " insert into weather_max (city_name, date, max_temp)"
                        + " values (?, ?, ?)";

                // create the mysql insert preparedstatement
                PreparedStatement preparedStmt = connToDatabase.prepareStatement(query);
                preparedStmt.setString(1, rs.getCityName());
                preparedStmt.setDate(2, java.sql.Date.valueOf(rs.getDate()));
                preparedStmt.setString(3, rs.getMax_temp() + "℃");

                // execute the preparedstatement
                preparedStmt.execute();

            }

            //put all the rainy days in a table
            for (ResultRain rs : resultInfo2) {

                // the mysql insert statement
                String query = " insert into weather_forecast (city_name, date, forecast)"
                        + " values (?, ?, ?)";

                // create the mysql insert preparedstatement
                PreparedStatement preparedStmt = connToDatabase.prepareStatement(query);
                preparedStmt.setString(1, rs.getCityName());
                preparedStmt.setDate(2, java.sql.Date.valueOf(rs.getDate()));
                preparedStmt.setString(3, rs.getWeather());

                // execute the preparedstatement
                preparedStmt.execute();

            }
            connToDatabase.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    //making the apicall for a certain city and returning
    //a string with the api call JSON info
    public String makeApiCall(int i) {
        String inline = "";
        HttpURLConnection conn;
        URL url;
        int responseCode;
        try {
            //takes the apicall as URL and sets a GET connection so we can
            //take the JSON data we need
            url = new URL("https://api.openweathermap.org/data/2.5/onecall?" + COORDS[i]
                    + "&exclude=current,minutely,hourly&units=metric&appid=5da1ddff83419bb29f1530adaf72384b");
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            //opens the URL connection
            conn.connect();
            responseCode = conn.getResponseCode();

            //populates inline with the scanner data
            inline = returnScanner(responseCode, url);
            //closes the URL connection
            conn.disconnect();
            return inline;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }

    }

}

