package com.example.weatherforecastspringboot.io.quicktype;

import com.fasterxml.jackson.annotation.*;

public class Daily {


    private long dt;
    private long sunrise;
    private long sunset;
    private Temp temp;
    private FeelsLike feelsLike;
    private long pressure;
    private long humidity;
    private double dewPoint;
    private double windSpeed;
    private long windDeg;
    private Weather[] weather;
    private long clouds;
    private double uvi;
    private Double rain;

    @JsonProperty("dt")
    public long getDt() { return dt; }
    @JsonProperty("dt")
    public void setDt(long value) { this.dt = value; }

    @JsonProperty("sunrise")
    public long getSunrise() { return sunrise; }
    @JsonProperty("sunrise")
    public void setSunrise(long value) { this.sunrise = value; }

    @JsonProperty("sunset")
    public long getSunset() { return sunset; }
    @JsonProperty("sunset")
    public void setSunset(long value) { this.sunset = value; }

    @JsonProperty("temp")
    public Temp getTemp() { return temp; }
    @JsonProperty("temp")
    public void setTemp(Temp value) { this.temp = value; }

    @JsonProperty("feels_like")
    public FeelsLike getFeelsLike() { return feelsLike; }
    @JsonProperty("feels_like")
    public void setFeelsLike(FeelsLike value) { this.feelsLike = value; }

    @JsonProperty("pressure")
    public long getPressure() { return pressure; }
    @JsonProperty("pressure")
    public void setPressure(long value) { this.pressure = value; }

    @JsonProperty("humidity")
    public long getHumidity() { return humidity; }
    @JsonProperty("humidity")
    public void setHumidity(long value) { this.humidity = value; }

    @JsonProperty("dew_point")
    public double getDewPoint() { return dewPoint; }
    @JsonProperty("dew_point")
    public void setDewPoint(double value) { this.dewPoint = value; }

    @JsonProperty("wind_speed")
    public double getWindSpeed() { return windSpeed; }
    @JsonProperty("wind_speed")
    public void setWindSpeed(double value) { this.windSpeed = value; }

    @JsonProperty("wind_deg")
    public long getWindDeg() { return windDeg; }
    @JsonProperty("wind_deg")
    public void setWindDeg(long value) { this.windDeg = value; }

    @JsonProperty("weather")
    public Weather[] getWeather() { return weather; }
    @JsonProperty("weather")
    public void setWeather(Weather[] value) { this.weather = value; }

    @JsonProperty("clouds")
    public long getClouds() { return clouds; }
    @JsonProperty("clouds")
    public void setClouds(long value) { this.clouds = value; }

    @JsonProperty("uvi")
    public double getUvi() { return uvi; }
    @JsonProperty("uvi")
    public void setUvi(double value) { this.uvi = value; }

    @JsonProperty("rain")
    public Double getRain() { return rain; }
    @JsonProperty("rain")
    public void setRain(Double value) { this.rain = value; }



}
