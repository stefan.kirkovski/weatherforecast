package com.example.weatherforecastspringboot.io.quicktype;

import com.fasterxml.jackson.annotation.*;

public class ApiData {

    private long lat;
    private double lon;
    private String timezone;
    private long timezoneOffset;
    private Daily[] daily;

    @JsonProperty("lat")
    public long getLat() { return lat; }
    @JsonProperty("lat")
    public void setLat(long value) { this.lat = value; }

    @JsonProperty("lon")
    public double getLon() { return lon; }
    @JsonProperty("lon")
    public void setLon(double value) { this.lon = value; }

    @JsonProperty("timezone")
    public String getTimezone() { return timezone; }
    @JsonProperty("timezone")
    public void setTimezone(String value) { this.timezone = value; }

    @JsonProperty("timezone_offset")
    public long getTimezoneOffset() { return timezoneOffset; }
    @JsonProperty("timezone_offset")
    public void setTimezoneOffset(long value) { this.timezoneOffset = value; }

    @JsonProperty("daily")
    public Daily[] getDaily() { return daily; }
    @JsonProperty("daily")
    public void setDaily(Daily[] value) { this.daily = value; }
}

