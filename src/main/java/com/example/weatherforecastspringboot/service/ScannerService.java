package com.example.weatherforecastspringboot.service;

import com.example.weatherforecastspringboot.ApiCall;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URL;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service
public class ScannerService {

    public String returnScanner(int responseCode, URL url) {
        String inline = "";
        if (responseCode != 200) {
            throw new RuntimeException("HttpResponseCode: " + responseCode);
        } else {
            try {
                //Next part of the functionality

                Scanner sc = new Scanner(url.openStream());
                while (sc.hasNext()) {
                    inline += sc.nextLine();
                }
                sc.close();
                return inline;
            } catch (IOException ex) {
                Logger.getLogger(ApiCall.class.getName()).log(Level.SEVERE, null, ex);
                return null;
            }
        }
    }
}
