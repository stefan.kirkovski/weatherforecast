package com.example.weatherforecastspringboot.service;


import com.example.weatherforecastspringboot.model.WeatherMax;
import com.example.weatherforecastspringboot.io.quicktype.ApiData;
import com.example.weatherforecastspringboot.io.quicktype.Converter;
import com.example.weatherforecastspringboot.io.quicktype.Daily;
import com.example.weatherforecastspringboot.io.quicktype.Temp;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class TempMaxService {

    public List<WeatherMax> returnCityDateMax(String jsonString) {
        try {

            //We take the data from a JSON string, convert it to data we can use
            //and then move on to the iterator to check for the next 5 days
            ApiData data = Converter.fromJsonString(jsonString);
            Daily[] daily = data.getDaily();
            long lat = data.getLat();
            double lon = data.getLon();
            Temp temp;
            List<WeatherMax> res = new ArrayList<>();
            int j = 0;
            Date date = new Date();
            LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

            //We need to get the next 5 days, so we always start from tomorrow
            for (int i = 1; i < 6; i++) {

                temp = daily[i].getTemp();
                //Checked with 20 Celsius, days ahead were too cold to check for 25
                if (temp.getMax() > 20) {
                    res.add(j++, new WeatherMax(checkCity(lat,lon), (localDate.plusDays(i)), Double.toString(temp.getMax()) + "℃"));
                }
            }
            return res;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public String checkCity(long lat, double lon) {

        if (lat == 42 && lon == 21.43) {
            return "Skopje";
        } else if (lat == 42 && lon == 20.97) {
            return "Tetovo";
        } else if (lat == 41 && lon == 21.34) {
            return "Bitola";
        } else {
            return "Invalid City";
        }

    }


}
