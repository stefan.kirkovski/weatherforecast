package com.example.weatherforecastspringboot.service;

import com.example.weatherforecastspringboot.ResultRain;
import com.example.weatherforecastspringboot.io.quicktype.ApiData;
import com.example.weatherforecastspringboot.io.quicktype.Converter;
import com.example.weatherforecastspringboot.io.quicktype.Daily;
import com.example.weatherforecastspringboot.io.quicktype.Weather;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;

@Service
public class RainService {


    public ArrayList<ResultRain> returnCityDateWeather(String jsonString, String cityName) {
        try {

            //We take the data from a JSON string, convert it to data we can use
            //and then move on to the iterator to check for the next 5 days
            ApiData data = Converter.fromJsonString(jsonString);
            Daily[] daily = data.getDaily();
            long lat = data.getLat();
            double lon = data.getLon();
            Weather[] weather;
            String forecast;
            ArrayList<ResultRain> res = new ArrayList<>();
            int j = 0;
            Date date = new Date();
            LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

            //We need to get the next 5 days, so we always start from tomorrow
            for (int i = 1; i < 6; i++) {

                weather = daily[i].getWeather();
                forecast = weather[0].getMain();
                //Check if the weather forecast equals Rain
                if (forecast.contains("Rain")) {
                    res.add(j++, new ResultRain(cityName, (localDate.plusDays(i)), forecast));
                }
            }
            return res;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }
}
