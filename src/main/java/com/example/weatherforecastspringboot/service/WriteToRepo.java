package com.example.weatherforecastspringboot.service;

import com.example.weatherforecastspringboot.dao.WeatherMaxRepo;
import com.example.weatherforecastspringboot.model.WeatherMax;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WriteToRepo {


    @Autowired
    WeatherMaxRepo repo;

        public void write(List<WeatherMax> weatherMaxes){
        for(WeatherMax wm:weatherMaxes)
            repo.save(wm);
    }
}
