package com.example.weatherforecastspringboot.service;

import com.example.weatherforecastspringboot.ApiCall;
import com.example.weatherforecastspringboot.ResultMax;
import com.example.weatherforecastspringboot.io.quicktype.ApiData;
import com.example.weatherforecastspringboot.io.quicktype.Converter;
import com.example.weatherforecastspringboot.io.quicktype.Daily;
import com.example.weatherforecastspringboot.io.quicktype.Temp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service
public class MakeApiCall {


    @Autowired
    private ScannerService scannerService;

    public String makeApiCall(String name) {
        String inline = "";
        HttpURLConnection conn;
        URL url;
        int responseCode;

        try {
            //takes the apicall as URL and sets a GET connection so we can
            //take the JSON data we need
            url = new URL("https://api.openweathermap.org/data/2.5/onecall?" + coords(name)
                   + "&exclude=current,minutely,hourly&units=metric&appid=5da1ddff83419bb29f1530adaf72384b");
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            //opens the URL connection
            conn.connect();
            responseCode = conn.getResponseCode();

            //populates inline with the scanner data
            inline = scannerService.returnScanner(responseCode, url);
            //closes the URL connection
            conn.disconnect();
            return inline;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }

    }

    public String coords(String name){

        switch(name){
            case "Skopje": case "skopje":
                return "lat=42&lon=21.433331";
            case "Tetovo": case "tetovo":
                return "lat=42.010559&lon=20.97139";
            case "Bitola": case "bitola":
                return "lat=41.031109&lon=21.340281";
            default:
                return "lat=42&lon=21.433331";
        }
    }


}