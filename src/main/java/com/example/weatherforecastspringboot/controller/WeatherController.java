package com.example.weatherforecastspringboot.controller;


import com.example.weatherforecastspringboot.dao.WeatherMaxRepo;
import com.example.weatherforecastspringboot.service.TempMaxService;
import com.example.weatherforecastspringboot.service.MakeApiCall;
import com.example.weatherforecastspringboot.service.WeatherService;
import com.example.weatherforecastspringboot.model.WeatherMax;
import com.example.weatherforecastspringboot.service.WriteToRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController()
public class WeatherController {

    @Autowired
    private WeatherService weatherService;

    @Autowired
    private MakeApiCall makeApiCall;

    @Autowired
    private TempMaxService tempMaxService;

    @Autowired
    WeatherMaxRepo repo;

    @Autowired
    WriteToRepo writeToRepo;

    @PostMapping(path="/weeklydatafetch")
    public String writeToDB(@RequestBody CityNameObject cityName){

        return cityName.getCityName();
    }

    @GetMapping(path="/maxtemps/{cityName}")
    public List<WeatherMax> fetchMax(@PathVariable("cityName") String name){

        return tempMaxService.returnCityDateMax(makeApiCall.makeApiCall(name));
    }

    /*@GetMapping(path="/maxtemps/{cityName}")
    public List<WeatherMax> fetchMax(@RequestParam String name){

        return tempMaxService.returnCityDateMax(makeApiCall.makeApiCall(name));
    }*/

    @GetMapping(path = "/fetchData")
    public List<WeatherMax> readFromDB(){

        return repo.findAll();
    }

    @PostMapping(path = "/updateDB")
    public List<WeatherMax> updateDB(@RequestBody WeatherMax weatherMax){

        List<WeatherMax> weatherMaxes = tempMaxService.returnCityDateMax(makeApiCall.makeApiCall(weatherMax.getCityname()));
        writeToRepo.write(weatherMaxes);

        return weatherMaxes;
    }

}
