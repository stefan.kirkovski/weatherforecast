package com.example.weatherforecastspringboot.controller;


public class CityNameObject {


    String cityName;

    public CityNameObject(String cityName) {
        this.cityName = cityName;
    }

    public CityNameObject(){

    }
    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }
}
