package com.example.weatherforecastspringboot;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDate;

/**
 *
 * @author Stefan Kirkovski
 */
//@Entity
public class ResultRain {

    //@Id
    //@GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String cityName;
    private LocalDate date;
    private String weather;


    public ResultRain(String cityName, LocalDate date,String weather){
        this.cityName = cityName;
        this.date = date;
        this.weather = weather;
    }
    public ResultRain(){}

    /**
     * @return the cityName
     */
    public String getCityName() {
        return cityName;
    }

    /**
     * @return the date
     */
    public LocalDate getDate() {
        return date;
    }

    /**
     * @return the weather
     */
    public String getWeather() {
        return weather;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

}

