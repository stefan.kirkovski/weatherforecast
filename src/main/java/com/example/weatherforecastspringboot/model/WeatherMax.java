package com.example.weatherforecastspringboot.model;


import org.springframework.data.rest.core.annotation.RestResource;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;

@Entity
public class WeatherMax {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    public WeatherMax(String cityname, LocalDate date, String temp) {
        this.cityname = cityname;
        this.date = date;
        this.temp = temp;
    }

    public WeatherMax(){}

    private String cityname;

    private LocalDate date;
    private String temp;



    @Override
    public String toString() {
        return "weatherMax{" +
                "id=" + id +
                ", cityName='" + cityname + '\'' +
                ", date=" + date +
                ", temp='" + temp + '\'' +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCityname() {
        return cityname;
    }

    public void setCityname(String cityname) {
        this.cityname = cityname;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getTemp() {
        return temp;
    }

    public void setTemp(String temp) {
        this.temp = temp;
    }

}
