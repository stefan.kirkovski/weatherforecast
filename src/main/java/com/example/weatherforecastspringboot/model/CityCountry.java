package com.example.weatherforecastspringboot.model;

import org.springframework.data.rest.core.annotation.RestResource;

import javax.persistence.*;
import java.util.List;

@Entity
public class CityCountry {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    /*@OneToOne
    @JoinColumn(name = "city_fk")
    @RestResource(path="cityfkid", rel = "cityfk")*/
    private int id;

    private String city;
    private String country;

   @ManyToOne
    private List<WeatherMax> weatherMaxes;

    public List<WeatherMax> getWeatherMaxes() {
        return weatherMaxes;
    }

    public void setWeatherMaxes(List<WeatherMax> weatherMaxes) {
        this.weatherMaxes = weatherMaxes;
    }

    public CityCountry(String city, String country) {
        this.city = city;
        this.country = country;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
