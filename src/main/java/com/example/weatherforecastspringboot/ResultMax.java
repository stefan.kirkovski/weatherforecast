package com.example.weatherforecastspringboot;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDate;

/**
 *
 * @author Stefan Kirkovski
 */
//@Entity
public class ResultMax {



    //@Id
   // @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String cityName;
    private LocalDate date;
    private double max_temp;



    public ResultMax(String cityName, LocalDate date,double max_temp){
        this.cityName = cityName;
        this.date = date;
        this.max_temp = max_temp;
    }
    public ResultMax(){}

    @Override
    public String toString(){
        return String.format("City: "+ this.cityName + " Date: " + this.date + " Max temp: " + this.max_temp + "℃");
    }
    /**
     * @return the cityName
     */
    public String getCityName() {
        return cityName;
    }

    /**
     * @param cityName the cityName to set
     */
    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    /**
     * @return the date
     */
    public LocalDate getDate() {
        return date;
    }

    /**
     * @param date the date to set
     */
    public void setDate(LocalDate date) {
        this.date = date;
    }

    /**
     * @return the max_temp
     */
    public double getMax_temp() {
        return max_temp;
    }

    /**
     * @param max_temp the max_temp to set
     */
    public void setMax_temp(double max_temp) {
        this.max_temp = max_temp;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

}

