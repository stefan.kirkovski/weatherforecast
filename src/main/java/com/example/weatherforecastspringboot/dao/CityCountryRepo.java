package com.example.weatherforecastspringboot.dao;

import com.example.weatherforecastspringboot.model.CityCountry;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CityCountryRepo extends JpaRepository<CityCountry, Integer> {
}
