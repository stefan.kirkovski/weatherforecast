package com.example.weatherforecastspringboot.dao;

import com.example.weatherforecastspringboot.model.WeatherMax;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WeatherMaxRepo extends JpaRepository<WeatherMax, Integer> {
}
